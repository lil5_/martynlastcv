/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package martynlastcv;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Lucian I. Last
 */
public class TextToHtml {
	
	private String[] fileData;
	private ArrayList<String> aListData;
	
	/**
	 * will remove empty Strings in fileData Array
	 */
	public void removeEmplyLines() {
		// convert String[] to ArrayList<String>
				aListData = new ArrayList<String>();
				Collections.addAll(aListData, fileData);
				
				int i=0;
				while(i<aListData.size()) {
					((String) aListData.get(i)).trim();
					if(
						((String) aListData.get(i) ).trim().isEmpty()
					) {
						aListData.remove(i);
						i--;
					}
					i++;
				}
				
				// convert back to String[]
				fileData = (String[]) aListData.toArray(new String[0]);
				System.out.println("Empty Strings removed.");
	}

	public void setFileData(String[] fileData) {
		this.fileData = fileData;
	}
	
}
