/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package martynlastcv;

/**
 * will read .doc file 
 * and turn it into an Html table
 * then write it to a seperate .php file
 * @author Lucian I. Last
 */
public class MartynLastCV {
	
 /*
	DocFileEdit docFileEdit;
	TextToHtml textToHtml;
	OpenFileWindow openFileWindow;
	String filePath;
	*/

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		DocFileEdit docR = new DocFileEdit();
		docR.setFilePath("/home/usr555/New.doc");
		docR.readFile();
		docR.printFullFileData();
		
		PhpFileEdit phpW = new PhpFileEdit();
		phpW.setFileData(
			docR.getFileData() );
		phpW.setFilePath("/home/usr555/Old.php");
		
		phpW.writeFile();
	}
	
}
