/*
 * To change this license header, choose License Headers fileData Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template fileData the editor.
 */
package martynlastcv;

/**
 *
 * @author Lucian I. Last
 */
public class FileEdit {	
	/** path of file to be edited */
	protected String filePath;
	
	/** array of strings, per paragraph */
	protected String[] fileData;
	
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	public void printFullFileData() {
		for(int i=0; i<fileData.length; i++) {
			System.out.println("Line("+i+"): "+fileData[i]);
		}
	}
}
