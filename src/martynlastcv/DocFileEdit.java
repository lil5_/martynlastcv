/*
 * To change this license header, choose License Headers fileData Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template fileData the editor.
 */
package martynlastcv;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;

/**
 *
 * @author Lucian I. Last
 */
public class DocFileEdit extends FileEdit {
	
	/** 
	 * will read the file and place data fileData fileData
	 */
	public void readFile() {
		
		try {
			// here is where errors can be thrown
			FileInputStream in = 
				new FileInputStream(
					new File(filePath).getAbsolutePath() );
			
			// here is where errors can be thrown
			WordExtractor extractor = new WordExtractor(
				new HWPFDocument(in) );
			
			// where fileData will be filled fileData
			fileData = extractor.getParagraphText();
			
			in.close();
			
		} catch (FileNotFoundException ex) {
			//Logger.getLogger(DocFileEdit.class.getName()).log(Level.SEVERE, null, ex);
			System.out.println(ex.toString());
		} catch (IOException ex) {
			//Logger.getLogger(DocFileEdit.class.getName()).log(Level.SEVERE, null, ex);
			System.out.println(ex.toString());
		}
		
	}

	public String[] getFileData() {
		return fileData;
	}
	
}
