/*
 * To change this license header, choose License Headers fileData Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template fileData the editor.
 */
package martynlastcv;

import java.io.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lucian I. Last
 */
public class PhpFileEdit extends FileEdit {
	public void writeFile() {
		try {
			BufferedWriter out = new BufferedWriter(
				new FileWriter(
					new File(filePath) ) );
			
			for (String line : fileData) {
				out.write(line);
				out.newLine();
			}
			
			out.close();
		} catch (IOException ex) {
			Logger.getLogger(PhpFileEdit.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public void setFileData(String[] fileData) {
		this.fileData = fileData;
	}
	
}
